#!/bin/bash
set -o errexit
cd $SCRIPTS_FOLDER
# Poll until we can successfully connect to MongoDB
echo 'Connecting to MongoDB...'
node <<- 'EOJS'
require('p-wait-for')(function() {
	return new Promise(function (resolve) {
    if (process.env.MONGO_URL) {
      require('mongodb').MongoClient.connect(process.env.MONGO_URL, function(err, client) {
        const successfullyConnected = err == null;
        if (successfullyConnected) {
          client.close();
        }
        resolve(successfullyConnected);
      });
    } else {
      console.warn("No MONGO_URL has been set, continuing without checking the connection string");
      resolve(true);
    }
	});
}, 1000);
EOJS
echo 'Starting app...'
cd $APP_BUNDLE_FOLDER/bundle
exec "$@"