import { Random } from "meteor/random";
import { Meteor } from "meteor/meteor";

console.log(`Greetings from ${module.id}!`);

const randomlyThrowAnError = () => {
  Meteor.setTimeout(() => {
    try {
      Meteor.call("randomly-throwing-an-error", "this method does not exist!");
    } finally {
      randomlyThrowAnError();
    }
  }, Random.fraction() * 120 * 1000);
}

randomlyThrowAnError();
