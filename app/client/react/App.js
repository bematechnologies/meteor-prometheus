import React, { useEffect, useState } from 'react';

import {
  Container, Row, Col
} from 'react-bootstrap';

const App = () => {
  const [snapshot, setSnapshot] = useState([]);
  const [apps, setApps] = useState([]);
  const [logs, setLogs] = useState([]);

  const [isLoading, setLoading] = useState(true);

  const fetchData = () => {
    fetch('/snapshot')
      .then(async (response) => {
        let json = await response.json();

        if (json) {
          setSnapshot(json.snapshot);
          setApps(json.apps);
          setLogs(json.logs);
        }

        setLoading(false);
      }).catch((err) => {
        console.log(err);
        setLoading(false);
      })
  }

  useEffect(() => {
    setInterval(() => {
      fetchData();
    }, 5000);

    fetchData();
  }, []);

  if (isLoading) {
    return null;
  }

  return (
    <>
      <Container className='pt-3'>
        <Row>
          <Col xs='12'>
            <h4>Meteor Prometheus Engine</h4>
            <p className='mb-2'>Full exporter accessible at <a href={`${Meteor.absoluteUrl("/metrics")}`}>{`${Meteor.absoluteUrl("/metrics")}`}</a>.</p>
            <p className='mb-2'>To set up an app, install mdg:meteor-apm-agent and set the environmental variables listed below in your app:</p>
            <pre>
              <code>
                <div>{`process.env.APM_OPTIONS_ENDPOINT = ${Meteor.absoluteUrl("/collector")};`}</div>
                <div>{`process.env.APM_APP_ID = <APP_ID>;`}</div>
                <div>{`process.env.APM_APP_SECRET = <APP_SECRET>;`}</div>
              </code>
            </pre>
          </Col>
          {snapshot.length == 0 && <Col xs='12' className='mb-3'>No metrics found.</Col>}
          {snapshot.map((app, i) => (
            <React.Fragment key={`app-${i}`}>
              <Col xs='12'>
                <h6 className='mb-1'>{app.app}</h6>
                <div className='mb-1'>{`App exporter accessible at `}<a href={Meteor.absoluteUrl(`/metrics/${app.app}`)}>{Meteor.absoluteUrl(`/metrics/${app.app}`)}</a>.</div>
                <div className='basic-box'>
                  <Container fluid className='text-center'>
                    <Row>
                      <Col xs='4'>
                        <h6>Sessions</h6>
                        <h1 className='mb-0'>{app.sessions}</h1>
                      </Col>
                      <Col xs='4'>
                        <h6>CPU</h6>
                        <h1 className='mb-0'>{(app.cpu).toFixed(2)}<small>%</small></h1>
                      </Col>
                      <Col xs='4'>
                        <h6>Memory</h6>
                        <h1 className='mb-0'>{(app.mem).toFixed(2)}</h1>
                      </Col>
                    </Row>
                  </Container>
                </div>
              </Col>
            </React.Fragment>
          ))}
          <Col xs='12'>
            <h6>Registered Apps:</h6>
            {apps.length == 0 && <div className='mb-3'>No registered apps found.</div>}
            {apps.length > 0 && <pre>
              <code>
                {apps.map((app, i) => (
                  <div key={`app-${i}`} className='mb-1'>
                    {`APP_ID = ${app.name}; APP_SECRET = ${app.secret};`}
                  </div>
                ))}
              </code>
            </pre>}
          </Col>
          <Col xs='12'>
            <h6>To register an app:</h6>
            <pre>
              <code>
                {`process.env.APPS = <APP_ID>:<APP_SECRET>,<APP_ID2>:<APP_SECRET2>,...`}
              </code>
            </pre>
          </Col>
          <Col xs='12'>
            <h6>To set up basic auth:</h6>
            <pre>
              <code>
                {`process.env.BASIC_AUTH = <user>:<password>`}
              </code>
            </pre>
          </Col>
          <Col xs='12'>
            <h6>Recent Error Logs: (last 24 hours) (latest logs shown first)</h6>
            {logs.length == 0 && <p>No error logs found.</p>}
            {logs.length > 0 && <div className='basic-box px-0 py-0' style={{ maxHeight: "calc(100vh - 60px)", overflowY: "scroll" }}>
              {logs.reverse().map((log, i) => (
                <div key={`log-${i}`} className='p-3' style={i != 0 ? { borderTop: "1px solid var(--secondary)" } : {}}>
                  <pre className='pb-0 mb-0'>{log.log}</pre>
                </div>
              ))}
            </div>}
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default App;