import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';

import './styles/index.scss';

const { default: App } = require("./react/App");

Meteor.startup(() => {
  render(<App />, document.getElementById('react-root'));
});
