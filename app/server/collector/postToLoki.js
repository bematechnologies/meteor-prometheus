import { HTTP } from 'meteor/http';

// TODO
// swap this over to protobufs
// also should pool logs here and pass them along as a stream in intervals
// https://grafana.com/docs/loki/latest/api/#post-lokiapiv1push
const postToLoki = (log, type, labels) => {
  Meteor.defer(() => {
    const data = {
      streams: [
        {
          stream: {
            type,
            ...labels,
          },
          values: [
            // nanoseconds?
            [((new Date()).valueOf() * 1000000).toString(), log]
          ]
        }
      ]
    }

    try {
      HTTP.post(`${process.env.LOKI}/loki/api/v1/push`, {
        headers: {
          type: "application/json"
        },
        data
      });
    } catch (e) {
      console.warn(e);
    }
  });
}

export default postToLoki;