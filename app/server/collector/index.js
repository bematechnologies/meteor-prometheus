const bodyParser = require('body-parser');

import moment from 'moment';
import postToLoki from './postToLoki';

WebApp.connectHandlers.use('/collector', bodyParser.json());

// Listen to incoming HTTP requests (can only be used on the server).
WebApp.connectHandlers.use('/collector', (req, res, next) => {
  if (req.url == '/simplentp/sync') {
    res.writeHead(200);
    res.end(Date.now().toString());
    return;
  }

  let app = req.headers["kadira-app-id"];
  let secret = req.headers["kadira-app-secret"];

  // authenticate
  if (!app || !secret || !process.env.APPS.includes(`${app}:${secret}`)) {
    res.writeHead(200);
    res.end(`Recieved`);
    return;
  }

  if (app) {
    let body = req.body;
    // we only care about passed analytics that are metrics
    // ignore app stats and other metrics that kadira passes along
    if (!body || Object.keys(body).length == 0 || body.appStats) {
      res.writeHead(200);
      res.end(`Recieved`);
      return;
    }

    // clean up method metrics for easier prom exports
    if (body.methodMetrics.length == 0) {
      body.methodMetrics = [{ methods: [] }];
    }

    body.methodMetrics = body.methodMetrics.map((metrics) => {
      let count = 0;
      let errors = 0;
      let fetchedDocSize = 0;
      let sentMsgSize = 0;
      let wait = 0;
      let db = 0;
      let http = 0;
      let email = 0;
      let async = 0;
      let compute = 0;
      let total = 0;

      Object.keys(metrics.methods).forEach((method) => {
        let methodData = metrics.methods[method];
        count += (methodData.count || 0);
        errors += (methodData.errors || 0);
        fetchedDocSize += (methodData.fetchedDocSize || 0);
        sentMsgSize += (methodData.sentMsgSize || 0);
        wait += (methodData.wait || 0);
        db += (methodData.db || 0);
        http += (methodData.http || 0);
        email += (methodData.email || 0);
        async += (methodData.async || 0);
        compute += (methodData.compute || 0);
        total += (methodData.total || 0);
      });

      return {
        count,
        errors,
        fetchedDocSize,
        sentMsgSize,
        wait,
        db,
        http,
        email,
        async,
        compute,
        total,
      }
    });

    // clean up publication metrics for easier prom exports
    // clean up method metrics for easier prom exports
    if (body.pubMetrics.length == 0) {
      body.pubMetrics = [{ pubs: [] }];
    }

    body.pubMetrics = body.pubMetrics.map((metrics) => {
      let subs = 0;
      let unsubs = 0;
      let resTime = 0;
      let activeSubs = 0;
      let activeDocs = 0;
      let lifeTime = 0;
      let totalObservers = 0;
      let cachedObservers = 0;
      let createdObservers = 0;
      let deletedObservers = 0;
      let errors = 0;
      let observerLifetime = 0;
      let polledDocuments = 0;
      let oplogUpdatedDocuments = 0;
      let oplogInsertedDocuments = 0;
      let oplogDeletedDocuments = 0;
      let initiallyAddedDocuments = 0;
      let liveAddedDocuments = 0;
      let liveChangedDocuments = 0;
      let liveRemovedDocuments = 0;
      let polledDocSize = 0;
      let fetchedDocSize = 0;
      let initiallyFetchedDocSize = 0;
      let liveFetchedDocSize = 0;
      let initiallySentMsgSize = 0;
      let liveSentMsgSize = 0;
      let avgObserverReuse = 0;

      Object.keys(metrics.pubs).forEach((publication) => {
        pubData = metrics.pubs[publication];
        subs += (pubData.subs || 0);
        unsubs += (pubData.unsubs || 0);
        resTime += (pubData.resTime || 0);
        activeSubs += (pubData.activeSubs || 0);
        activeDocs += (pubData.activeDocs || 0);
        lifeTime += (pubData.lifeTime || 0);
        totalObservers += (pubData.totalObservers || 0);
        cachedObservers += (pubData.cachedObservers || 0);
        createdObservers += (pubData.createdObservers || 0);
        deletedObservers += (pubData.deletedObservers || 0);
        errors += (pubData.errors || 0);
        observerLifetime += (pubData.observerLifetime || 0);
        polledDocuments += (pubData.polledDocuments || 0);
        oplogUpdatedDocuments += (pubData.oplogUpdatedDocuments || 0);
        oplogInsertedDocuments += (pubData.oplogInsertedDocuments || 0);
        oplogDeletedDocuments += (pubData.oplogDeletedDocuments || 0);
        initiallyAddedDocuments += (pubData.initiallyAddedDocuments || 0);
        liveAddedDocuments += (pubData.liveAddedDocuments || 0);
        liveChangedDocuments += (pubData.liveChangedDocuments || 0);
        liveRemovedDocuments += (pubData.liveRemovedDocuments || 0);
        polledDocSize += (pubData.polledDocSize || 0);
        fetchedDocSize += (pubData.fetchedDocSize || 0);
        initiallyFetchedDocSize += (pubData.initiallyFetchedDocSize || 0);
        liveFetchedDocSize += (pubData.liveFetchedDocSize || 0);
        initiallySentMsgSize += (pubData.initiallySentMsgSize || 0);
        liveSentMsgSize += (pubData.liveSentMsgSize || 0);
        avgObserverReuse += (pubData.avgObserverReuse || 0);
      });

      return {
        subs,
        unsubs,
        resTime,
        activeSubs,
        activeDocs,
        lifeTime,
        totalObservers,
        cachedObservers,
        createdObservers,
        deletedObservers,
        errors,
        observerLifetime,
        polledDocuments,
        oplogUpdatedDocuments,
        oplogInsertedDocuments,
        oplogDeletedDocuments,
        initiallyAddedDocuments,
        liveAddedDocuments,
        liveChangedDocuments,
        liveRemovedDocuments,
        polledDocSize,
        fetchedDocSize,
        initiallyFetchedDocSize,
        liveFetchedDocSize,
        initiallySentMsgSize,
        liveSentMsgSize,
        avgObserverReuse,
      }
    });

    body.errors = body.errors.map((error) => {
      let log = `${moment().format("MM/DD/YYYY hh:mm:ss A")}: APM Error: ${error.appId}\n${JSON.stringify({
        appId: error.appId,
        app,
        host: body.host,
        name: error.name,
        type: error.type,
        subType: error.subType,
      }, null, 2)}\n${error.stacks.map((stack) => (`${stack.stack}\n`))}`;

      // simply log to console, aggregate logs how you see fit
      console.error(log);

      // optional transport to a loki client
      if (process.env.LOKI) {
        postToLoki(log, "error", {
          appId: error.appId,
          subType: error.subType,
          host: body.host
        });
      }

      // push log into cache
      Logs.insert({
        dt: new Date(),
        app,
        log,
      });

      return {
        count: error.count || 0
      }
    });

    if (body.errors.length == 0) {
      body.errors = [
        {
          count: 0
        }
      ]
    }

    // add timestamp
    body.dt = new Date();

    Metrics.upsert({ app, host: body.host }, {
      $set: body
    });
  }

  res.writeHead(200);
  res.end(`Recieved`);
});