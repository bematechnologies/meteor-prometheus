const metricsToGather = {
  "systemMetrics": {
    "sessions": {
      type: "sum",
      name: "sessions",
    },
    "newSessions": {
      type: "sum",
      name: "new_sessions",
    },
    "memory": {
      type: "avg",
      name: "memory",
    },
    "pcpu": {
      type: "avg",
      name: "cpu",
    }
  },
  "methodMetrics": {
    "count": {
      type: "sum",
      name: "method_count",
    },
    "errors": {
      type: "sum",
      name: "method_errors",
    },
    "total": {
      type: "avg",
      name: "method_response_time",
    },
    "fetchedDocSize": {
      type: "sum",
      name: "method_fetched_doc_size",
    },
    "sentMsgSize": {
      type: "sum",
      name: "method_sent_msg_size",
    }
  },
  "pubMetrics": {
    "subs": {
      type: "sum",
      name: "publication_subs",
    },
    "unsubs": {
      type: "sum",
      name: "publication_unsubs",
    },
    "errors": {
      type: "sum",
      name: "publication_errors",
    },
    "resTime": {
      type: "avg",
      name: "publication_response_time",
    },
  },
  "errors": {
    "count": {
      type: "sum",
      name: "errors",
    },
  }
}

// little helper function that runs to inialize values and counts
Object.keys(metricsToGather).forEach((kadiraMetric) => {
  Object.keys(metricsToGather[kadiraMetric]).forEach((metric) => {
    metricsToGather[kadiraMetric][metric].value = 0;
    metricsToGather[kadiraMetric][metric].count = 0;
  })
})

const generateExport = (data, app) => {
  let scrape = "";

  // helper to add proper metric naming to the prometheus export
  const addMetric = (title, value, labels) => {
    let metric = `${title}`;
    if (labels) {
      metric += '{';
      Object.keys(labels).forEach((label, i) => {
        if (i > 0) {
          metric += ',';
        }

        metric += `${label}="${labels[label]}"`;
      })

      metric += '}';
    }

    metric += ` ${value}\n`;

    scrape += metric;
  }

  // go ahead and gather all the metrics defined above in the config
  let runtimeMetrics = JSON.parse(JSON.stringify(metricsToGather));
  data.forEach((hostData) => {
    const { host } = hostData;

    addMetric("host", 1, { host, category: "systemMetrics", app });

    Object.keys(runtimeMetrics).forEach((kadiraMetric) => {
      if (hostData[kadiraMetric]) {
        hostData[kadiraMetric].forEach((stat) => {

          Object.keys(stat).forEach((metric) => {
            let collectorMetric = runtimeMetrics[kadiraMetric][metric];

            if (collectorMetric) {
              collectorMetric.value += (stat[metric] || 0);
              collectorMetric.count += 1;

              addMetric(collectorMetric.name, stat[metric] || 0, { host, category: kadiraMetric, type: collectorMetric.type, app });
            }
          });
        })
      }
    })
  });

  Object.keys(runtimeMetrics).forEach((kadiraMetric) => {
    Object.keys(runtimeMetrics[kadiraMetric]).forEach((metric) => {
      let metricData = runtimeMetrics[kadiraMetric][metric];
      let value = metricData.value;
      if (metricData.type == 'avg' && metricData.count) {
        value = metricData.value / metricData.count;
      }

      addMetric(`${metricData.name}_${metricData.type}`, value, { category: kadiraMetric, type: metricData.type, app });
    });
  });

  let numberOfHosts = data.length;
  addMetric("host_sum", numberOfHosts);

  return scrape;
}

export default generateExport;