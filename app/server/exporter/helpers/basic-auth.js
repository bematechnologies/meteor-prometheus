const basicAuth = (req) => {
  if (!process.env.BASIC_AUTH) {
    return true;
  }

  const auth = req.headers['authorization'];
  if (!auth) {
    return false;
  }

  const tmp = auth.split(' ');
  if (!tmp[1]) {
    return false;
  }

  const buf = Buffer.from(tmp[1], 'base64');
  const cleanedAuth = buf.toString("ascii");
  if (cleanedAuth == process.env.BASIC_AUTH) {
    return true;
  }
  
  return false;
}

export default basicAuth;