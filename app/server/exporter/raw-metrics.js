// helper endpoint to see raw metrics for an app

const { default: basicAuth } = require("./helpers/basic-auth");

WebApp.connectHandlers.use('/raw-metrics', (req, res, next) => {
  const authenticated = basicAuth(req);
  if (!authenticated) {
    res.statusCode = 401;
    res.setHeader('WWW-Authenticate', 'Basic realm="Secure Area"');
    res.end('<html><body>Authentication required.</body></html>');
    return;
  }

  let app = req.url.substring(1);

  if (!app) {
    res.writeHead(500);
    res.end("Please provide an app in the url!");
    return
  }

  let data = {};
  if (app) {
    data = Metrics.find({ app }).fetch();
  }

  res.writeHead(200, {
    "Content-Type": "application/json"
  });
  res.end(JSON.stringify(data));
});
