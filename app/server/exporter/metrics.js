const { default: generateExport } = require("./helpers/generateExport");
const { default: basicAuth } = require("./helpers/basic-auth");

// main metrics endpoint for prom export
WebApp.connectHandlers.use('/metrics', (req, res, next) => {
  const authenticated = basicAuth(req);
  if (!authenticated) {
    res.statusCode = 401;
    res.setHeader('WWW-Authenticate', 'Basic realm="Secure Area"');
    res.end('<html><body>Authentication required.</body></html>');
    return;
  }

  let prometheusExport = "";

  let app = req.url.substring(1);
  if (app) {
    try {
      let data = Metrics.find({ app }).fetch();
      prometheusExport = generateExport(data, app);
    } catch (e) {
      console.error(e);
    }
  } else if (!app) {
    process.env.APPS.split(',').forEach((appConfig) => {
      const appName = appConfig.split(':')[0];

      try {
        let data = Metrics.find({ app: appName }).fetch();
        prometheusExport += generateExport(data, appName);
      } catch (e) {
        console.error(e);
      }
    });
  }

  res.writeHead(200);
  res.end(prometheusExport);
});
