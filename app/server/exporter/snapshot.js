import basicAuth from './helpers/basic-auth';

// snapshot for home page
WebApp.connectHandlers.use('/snapshot', (req, res, next) => {
  const authenticated = basicAuth(req);
  if (!authenticated) {
    res.statusCode = 401;
    res.setHeader('WWW-Authenticate', 'Basic realm="Secure Area"');
    res.end('<html><body>Authentication required.</body></html>');
    return;
  }

  let apps = [];
  let snapshot = [];
  if (process.env.APPS) {
    let split = process.env.APPS.split(',');
    apps = split.map((app) => {
      let appSplit = app.split(":");

      return {
        name: appSplit[0],
        secret: appSplit[1],
        logs: Logs.find({ app: appSplit[0] }, { sort: { dt: 1 } }).fetch()
      }
    });

    apps.forEach((app) => {
      try {
        let appData = Metrics.find({ app: app.name }).fetch();

        let snapshotData = {
          app: app.name,
          sessions: 0,
          cpu: 0,
          mem: 0
        }

        let cpuCount = 0;
        let summedCpu = 0;
        appData.forEach((host) => {
          snapshotData.sessions += host.systemMetrics.reduce((acc, obj) => {
            return acc + obj.sessions;
          }, 0);

          host.systemMetrics.forEach((metric) => {
            cpuCount += 1;
            summedCpu += metric.pcpu;
          }, 0);

          snapshotData.mem += host.systemMetrics.reduce((acc, obj) => {
            return acc + obj.memory;
          }, 0);
        });

        if (cpuCount > 0) {
          snapshotData.cpu = summedCpu / cpuCount;
        }

        snapshot.push(snapshotData)
      } catch (e) {
        console.error(e);
      }
    })
  }

  res.writeHead(200, {
    Content: "application/json"
  });

  res.end(JSON.stringify({
    snapshot,
    apps,
    logs: Logs.find({}, { sort: { dt: 1 } }).fetch()
  }));
});
