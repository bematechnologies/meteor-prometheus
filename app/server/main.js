Metrics = new Mongo.Collection('metrics');
Logs = new Mongo.Collection('logs');

require("./collector");
require("./exporter");
require("./cleaner");