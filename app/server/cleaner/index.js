import { Meteor } from 'meteor/meteor';
import moment from 'moment';

// little cleaner to clear out old stagnent host metrics
const cleanOldMetricData = () => {
  Metrics.remove({
    dt: {
      $lte: moment().add(-30, 'seconds')
    }
  });

  Logs.remove({
    dt: {
      $lte: moment().add(-1, 'days')
    }
  });

  Meteor.setTimeout(cleanOldMetricData, 15000);
}

cleanOldMetricData();