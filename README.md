### Meteor Prometheus

#### Overview
Meteor Prometheus is meant to be an engine between your app and a prometheus scraper that translate APM agent data into scrapeable prometheus metrics.

![dashboard](images/index.png)

#### Why Meteor Prometheus
The data provided by the APM agent is awesome! However hosting your own Kadira server is a hassle and while Kadira is great, it's in need of some upgrades. So I set out to make a translation layer between the APM agent data and Prometheus. With this translation layer, we can then use the awesome tools provided by Promtheus and Graphana to create Dashboards to monitor our applications and use Alertmanager for notification if something is to go wrong.

**Update below, this was the original design...**
To make this happen, I did make some design decisions that you should be aware of with this project.. This is because Prometheus is your datastore. This engine simply caches the most recent metrics, cleans them if they become stale, and this data either can get scraped (or not). The second decision I made is around logging errors. Errors are simply logged to the console when recieved and we provide an output of the most recent 100 cached logs on the dashboard as a niceity. Internally we use a log exporter to a centralized GrayLog server to capture these logs but you can choose what you like to do with those logs as you see fit. I did add an optional integration with Loki though if you want to use that. It is turned on in the development stack. Lastly is authorization. Like most Prometheus exporters, the best way to secure the metrics is to secure the internal network. This is the recommended way of doing this. However I also built in support for Basic Authentication if you choose to do so. This will cause the UI to be locked behind Basic Auth as well as the scrape endpoints to be locked behind Basic Auth as well. The development stack is setup like this so you can see how it works. Prometheus can be set up to scrape over basic auth as can be seen in our `prometheus.yml` file.

**12/14/2020**
We started aggregating to many metrics for a single host to keep up with so I introduced Mongo to act as a metric cache to aggregate metrics from multiple hosts. I also now persist logs in Mongo since we have a store there cause why not?

#### Getting Started
First set the environment and run an instance of Meteor Prometheus using the hosting service of your choice.

Example Environment:
```
BASIC_AUTH: <user>:<password> (not necessary if you have secured your network)
APPS: <app-id>:<app-secret>,<app-id-2>;<app-secret-2>....
```

Next on your Meteor App(s):

First install the latest apm agent:
```
meteor add mdg:meteor-apm-agent
```

Next set the proper environmental variable to communicate to your running meteor-prometheus instance
```
APM_OPTIONS_ENDPOINT: https://<your-meteor-prometheus-endpoint>/collector
APM_APP_ID: <app-id>
APM_APP_SECRET: <app-secret>
MONGO_URL: <we-like-to-use-onboard-mongo-for-this>
```

If successful you will see on your app:
```
Meteor APM: completed instrumenting the app
```

And you will see metrics streaming into your Meteor Prometheus dashboard as well.

**Prometheus:**

Scrape endpoints are accessible at `/metrics` and `/metrics/<app-name>`. You can choose to grab everything or simply metrics from one app at a time.

**Loki:**

Optionally set a `LOKI` environment variable to have error logs transported to the Loki endpoint of your choosing. Please note Loki endpoints are not secured by design so you will need to secure the network as you see fit.

#### Developing
```
docker-compose -f dev.docker-compose.yml
```

**What this does:**

* Starts 7 services and 1 mongo instance
* This includes:
    - Prometheus (http://localhost:9090)
    - AlertManager 
    - Graphana (http://localhost:4000)
    - Meteor-prometheus (http://localhost:3000)
    - A test meteor app that exports data (http://localhost:3001)
    - cadvisor

This stands up a full Prometheus/Graphana monitoring stack with Meteor Prometheus and a very basic meteor application that is exporting APM data from it. It also includes template dashboards for you to use in your own instance of Graphana built off of the export data from Meteor Prometheus.

#### Roadmap
* Export a few more example dashboards in Prometheus
* Testing

Happy hacking ;)
